<?php
ini_set('display_errors', 0);
error_reporting(E_ALL);
date_default_timezone_set('UTC');

require __DIR__ . '/app/config.php';

require ROOT . '/vendor/autoload.php';
require ROOT . '/app/Core/Route.php';

$conf = new Config($config);
$route = new RouteLogic($conf);
$route->getFile();

?>
<form method="POST" action="#">
	<input type="text" name="name" >
	<button type="submit">Send</button>
</form>
