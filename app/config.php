<?php
define('VER', 1);
define('HOST', $_SERVER['HTTP_HOST']);
define('ROOT', dirname(__DIR__));
define('ENV', file_exists(ROOT . '/.local') ? 'local' : 'prod');

$config = array(
	'route' => array(
		'auth' => array('guest'=> 'authGuest', 'vk', 'facebook'),
		'token' => array('get', 'refresh', 'logout'),
		'device' => array('post', 'update'),
		'user' => array('get'=> 'userGet', 'post'=>'userPost', 'update'=> 'userUpdate')
	),
	'database' => array(
		'host' => '',
		'user' => '',
		'pass' => '',
		'db' => ''
	),
	'google' => array(),
	'ios' => array()
);

if(ENV=='local') {
	$config['database'] = array(
		'host' => 'localhost',
		'user' => 'root',
		'pass' => '',
		'db' => 'hiro'
	);
};

class Config {
	private $arr;
	public function __construct($arr) {
	$this->arr = $arr;
	}

	public function get($type) {
		if(isset($this->arr[$type])) {
			return $this->arr[$type];
		}
	}
}