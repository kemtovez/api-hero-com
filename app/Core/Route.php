<?php
class Route {
	private $url;
	private $get;
	private $post;
	public function __construct($url, $get, $post) {
		$this->url = $url;
		$this->get = $get;
		$this->post = $post;
	}
	public function getUrl() {
		return $this->url;
	}
	public function getPost() {
		return $this->post;
	}
	public function getGet() {
		return $this->get;
	}
}

class RouteLogic {
	protected $config;
	public function __construct(Config $config) {
	$this->config = $config;
	}
	public function getFile() {
		$settingRoute = $this->config->get('route');
		$server = $this->getServer();
		$key = '';
		foreach ($server->getUrl() as $oneUrl) {
			if(isset($settingRoute[$oneUrl])) {
				if (file_exists(ROOT .'/app/v'.VER. '/'.$oneUrl.'/'.$oneUrl.'.class.php')) {
					require ROOT .'/app/v'.VER. '/'.$oneUrl.'/'.$oneUrl.'.class.php';
					$key = $oneUrl;
				}
			};
			if(isset($settingRoute[$key][$oneUrl])) {
				if (file_exists(ROOT .'/app/v'.VER. '/'.$key.'/'.$settingRoute[ $key ][ $oneUrl ].'.class.php')) {
					require ROOT .'/app/v'.VER. '/'.$key.'/'.$settingRoute[ $key ][ $oneUrl ].'.class.php';
				}
			}
		}

	}
	public function getServer() {
		if(isset($_SERVER['REQUEST_URI'])) {
			$uri = $_SERVER['REQUEST_URI'];
			$get = array();
			if(isset($_GET)) {
				$get = $_GET;
				unset($get['q']);
				$getString = explode('?', $_SERVER['REQUEST_URI']);
				$getString = array_diff($getString, array(''));
				$uri = $getString[0];
			}
			$uri_arr = explode('/', $uri);
			$uri_arr = array_diff($uri_arr, array(''));
			$params = array();
			foreach ($uri_arr as $line_url) {
				preg_match_all("%[a-zA-Z0-9_-]%", $line_url, $m, PREG_SET_ORDER);
				if (count($m)) {
					$params[] = $line_url;
				}
			}
			return new Route($params, $get, $_POST);
		}
	}
}